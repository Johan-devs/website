$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval:2000
        
    });
    $('#reservaModal').on('show.bs.modal', function(e){
        $('.btn-reserva').removeClass('btn-primary');
        $('.btn-reserva').addClass('btn-secondary');
        $('.btn-reserva').prop('disable',true);
    });
    $('#reservaModal').on('hidden.bs.modal', function(e){
        $('.btn-reserva').removeClass('btn-secondary');
        $('.btn-reserva').addClass('btn-primary');
        $('.btn-reserva').prop('disable',false);

    });
  });